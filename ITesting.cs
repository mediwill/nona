﻿namespace Nona {
    public interface ITesting {
        void GetTestingConf();
        (bool, string) RunTesting();
    }
}
