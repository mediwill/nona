﻿namespace Nona
{

    // crawler interface
    public interface ISpider {

        // Get the configurations for each crawler.
        void GetSpiderConf();

        // Execute crawling.
        (bool, int, int) Crawl();

        // Execute crawling, scraping or both.
        // (bool, int, int) Execute();
    }
}
