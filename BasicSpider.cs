﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

using YamlDotNet.RepresentationModel;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.Runtime;

using SimpleLog;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Security.Cryptography;

namespace Nona {

    // result of spider
    public struct SpiderResult{ 

    }

    // spider base abstract class
    public abstract class BasicSpider : IDisposable{

        // basic setting
        protected string crawlerName;                       // crawler name
        protected string homeDirectory;                     // home directory
        protected string outputDirectory;                   // output directory
        protected string startUrl;                          // start URL
        protected bool restartMode = false;                 // resume mode (default is false)
        // log
        protected Log log;
        // YAML
        protected StreamReader input;                       // file input
        protected YamlStream yaml;                          // YAML document
        protected YamlMappingNode yamlMappingNode;          // mapping node
        protected YamlScalarNode yamlScalarNode;            // scalar node
        protected YamlSequenceNode yamlSequenceNode;        // sequence node
        protected string yamlValue;                         // YAML value
        // HTML
        protected string html;                              // HTML
        protected string htmlDirectory;                     // HTML directory
        protected string dietHtml;                          // diet HTML
        protected string dietHtmlDirectory;                 // diet HTML directory
        protected StreamReader htmlReader;                  // HTML StreamReader
        protected StreamWriter dietHtmlWriter;              // diet HTML StreamWriter
        protected StreamReader dietHtmlReader;              // diet HTML StreamReader
        // CSV
        protected string csv;                               // CSV
        protected string csvDirectory;                      // CSV directory
        protected StreamWriter csvWriter;                   // CSV StreamWriter
        // TSV
        protected string tsv;                               // TSV
        protected string tsvDirectory;                      // TSV directory
        protected StreamWriter tsvWriter;                   // TSV StreamWriter
        // Answers
        protected string answersDirectory;                  // Answers
        // Selenium Chrome driver
        protected ChromeOptions options;                    // Chrome driver options
        protected ChromeDriver driver;                      // Chrome driver
        protected string windowSize;                        // windows size
        protected ReadOnlyCollection<string> allHandles;    // all window handles
        protected string listWindow;                        // list window
        protected string currentWindow;                     // current window
        protected string newWindow;                         // new screen handle
        protected IWebElement element = null;               // element
        protected ReadOnlyCollection<IWebElement> elements = null; // elements
        // Others
        protected int intervalTime;                         // interval (milliseconds)
        protected int numberOfClickRetries;                 // number of retries
        protected int maxError;                             // maximum number of errors
        protected int pageNumber;                           // page number
        protected int lastPageNumber = 1;                   // last page number
        protected int lastFileNumber = 1;                   // last file number
        protected int indexInPage = 1;                      // index within the page
        protected int fileNumber = 1;                       // file number
        protected int restartFiledNumber = 1;               // resume file number
        protected int restartPageNumber = 1;               // resume page number
        protected int maxFileNumber = 1;                    // max file number
        protected int perPage = 20;                         // number of files in one page
        // AWS
        protected string awsRegion;                         // aws region
        protected string accessKey;                         // access key
        protected string secretKey;                         // secret key
        protected BasicAWSCredentials credentials;          // credentials
        protected AmazonS3Client s3Client;                  // S3 client
        // proxy
        protected Proxy proxy;
        protected bool useProxy = false;
        protected string proxyIp;
        protected string proxyPort;
        // execute mode
        protected string executeMode = "crawl";
        // debug mode
        protected bool debugMode = false;
        // show scrollbars
        protected bool hideScrollbars = true;
        // results
        public bool crawlResult = true;
        public int  crawlTotal = 0;
        public int  crawlError = 0;
        public bool scrapeResult = true;
        public int  scrapeTotal = 0;
        public int  scrapeError = 0;
        // whether the Dispose method has been called
        private bool disposedValue = false;

        // constructor
        public BasicSpider(string crawlerName, bool headless = true) {
            // Set the crawler name.
            this.crawlerName = crawlerName;

            // Read the global configuration file.
            ReadYaml("GlobalConf.yml");
            GetGlobalConf();

			// Create directories.
			// log
			Directory.CreateDirectory($"{homeDirectory}{outputDirectory}/log");
            // html
            htmlDirectory = $"{homeDirectory}{outputDirectory}/html/{this.crawlerName}";
			Directory.CreateDirectory(htmlDirectory);
            // diet html
            dietHtmlDirectory = $"{homeDirectory}{outputDirectory}/dietHtml/{this.crawlerName}";
			Directory.CreateDirectory(dietHtmlDirectory);
            // csv
            csvDirectory = $"{homeDirectory}{outputDirectory}/csv/{this.crawlerName}";
			Directory.CreateDirectory(csvDirectory);
            // tsv
            tsvDirectory = $"{homeDirectory}{outputDirectory}/tsv/{this.crawlerName}";
			Directory.CreateDirectory(tsvDirectory);
            // answers
            answersDirectory = $"{homeDirectory}answers/{this.crawlerName}";

            // Set the log.
            log = new Log(this.crawlerName, $"{homeDirectory}{outputDirectory}/log/");

            // Chrome driver
            options = new ChromeOptions();
            options.AddArgument($"window-size={windowSize}");
            if (headless) {
                options.AddArgument("headless");
            }
            if (hideScrollbars) {
                options.AddArgument("--hide-scrollbars");
            }
            //options.AddArgument("--no-sandbox");
            //options.AddArgument("--disable-dev-shm-usage");
            //options.AddArgument("--disable-gpu");

            // proxy
            if (useProxy) {
                proxy = new Proxy {
                    Kind = ProxyKind.Manual,
                    IsAutoDetect = false,
                    HttpProxy = $"{proxyIp}:{proxyPort}",
                    SslProxy = $"{proxyIp}:{proxyPort}"
                };
                options.Proxy = proxy;
            }

            // AWS S3
            credentials = new BasicAWSCredentials(accessKey, secretKey);
            var region = RegionEndpoint.GetBySystemName(awsRegion);
            s3Client = new AmazonS3Client(credentials, region);
        }
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                // free managed resources
            }

            if (disposedValue == false) {
                // free unmanaged resources
                driver.Quit();
                disposedValue = true;
            }
        }

        ~BasicSpider() {
            Dispose(false);
        }

        // property
        protected int IntervalTime {
            set {
                if (value > 2000) { 
                    intervalTime = value;                    
                } else {
                    intervalTime = 2000;
                }
            }
            get {
                return intervalTime;
            }
        }

        // Read the YAML file.
        protected void ReadYaml(string yamlFileName, string yamlFileDirectory = "./") {
            input = new StreamReader($"{yamlFileDirectory}{yamlFileName}", Encoding.UTF8);
            yaml = new YamlStream();
            yaml.Load(input);
            yamlMappingNode = (YamlMappingNode)yaml.Documents[0].RootNode;
        }

        // Get the values of the global configuration file.
        protected void GetGlobalConf() {
            OperatingSystem os = Environment.OSVersion;

            // home directory
            if (os.Platform == PlatformID.Unix) {
                homeDirectory = GetYamlMappingValue("LINUX_HOME_DIR", yamlMappingNode);
		        Console.WriteLine("System is Unix.");
            } else {
                homeDirectory = GetYamlMappingValue("WINDOWS_HOME_DIR", yamlMappingNode);
		        Console.WriteLine("System is Windows.");
            }

            // output directory
            outputDirectory = GetYamlMappingValue("OUTPUT_DIR", yamlMappingNode);

            // interval time (milliseconds)
            string intervalTimeString = GetYamlMappingValue("INTERVAL_TIME", yamlMappingNode);
            IntervalTime = int.Parse(intervalTimeString);

            // number of retries
            string numberOfClickRetriesString = GetYamlMappingValue("NUMBER_OF_CLICK_RETRIES", yamlMappingNode);
            numberOfClickRetries = int.Parse(numberOfClickRetriesString);
            
            // maximum number of errors
            // (When the number of errors reaches the maximum number of errors, crawling is terminated.)
            string maxErrorCountString = GetYamlMappingValue("MAX_ERROR_COUNT", yamlMappingNode);
            maxError = int.Parse(maxErrorCountString);

            // Chrome Headless window size
            windowSize = GetYamlMappingValue("WINDOW_SIZE", yamlMappingNode);

            // number of displays per page
            string perPageString = GetYamlMappingValue("PER_PAGE", yamlMappingNode);
            perPage = int.Parse(perPageString);

            // access key
            accessKey = GetYamlMappingValue("ACCESS_KEY", yamlMappingNode);

            // secret key
            secretKey = GetYamlMappingValue("SECRET_KEY", yamlMappingNode);

            // aws_region
            awsRegion = GetYamlMappingValue("AWS_REGION", yamlMappingNode);

            // use proxy
            string useProxyString = GetYamlMappingValue("USE_PROXY", yamlMappingNode);
            int useProxyInt = int.Parse(useProxyString);
            if (useProxyInt == 1) { 
                useProxy = true;    
            } else { 
                useProxy = false;                    
            }
            // proxy server IP address
            proxyIp = GetYamlMappingValue("PROXY_IP", yamlMappingNode);
            // proxy server port number
            proxyPort = GetYamlMappingValue("PROXY_PORT", yamlMappingNode);

            // debug mode
            debugMode = GetYamlMappingBoolValue("DEBUG_MODE", yamlMappingNode);

            // hide scrollbars
            hideScrollbars = GetYamlMappingBoolValue("HIDE_SCROLLBARS", yamlMappingNode);
        }

        // Get the string value of mapping from the YAML file.
        protected string GetYamlMappingValue(string key, YamlMappingNode yamlMappingNode) {
            yamlScalarNode = (YamlScalarNode)yamlMappingNode.Children[new YamlScalarNode(key)];
            yamlValue = yamlScalarNode.Value;
            return yamlValue;
        }

        // Get Sequence values from the YAML file.
        protected List<string> GetYamlSequenceValues(string key, YamlMappingNode yamlMappingNode) {
            List<string> yamlValues = new List<string>();

            var nodes = (YamlSequenceNode)yamlMappingNode.Children[new YamlScalarNode(key)];
            foreach (var node in nodes) {
                yamlValues.Add(node.ToString());
            }

            return yamlValues;
        }

        // Get the boolean value of mapping from the YAML file.
        protected bool GetYamlMappingBoolValue(string key, YamlMappingNode yamlMappingNode) {
            bool debugMode;
            yamlScalarNode = (YamlScalarNode)yamlMappingNode.Children[new YamlScalarNode(key)];
            yamlValue = yamlScalarNode.Value;

            switch (yamlValue) {
                case "true":
                case "True":
                case "TRUE":
                case "1":
                    debugMode = true;
                    break;
                default:
                    debugMode = false;
                    break;
            }
            return debugMode;
        }

        // Get the start page.
        protected void GetStartPage(string url) {
            driver.Url = url;
            log.Write($"{crawlerName} driver.Url = {url}");
        }

        // Get the page.
        protected void GetPage(string url) {
            Sleep(intervalTime);
            driver.Url = url;
            log.Write($"{crawlerName} driver.Url = {url}");
        }
        
        // Get local html
        protected void GetLocalPage(string filePath) {
            driver.Url = $"file:///{filePath}";
            log.Write($"{crawlerName} driver.Url = {filePath}");
        }

        // Get HTML.
        protected void GetHtml() {
            Sleep(intervalTime);
            html = driver.PageSource;
        }

        // Click the element.
        protected bool ClickElement(string cssSelector) {
            for (int i = 0; i < numberOfClickRetries; i++) { 
                Sleep(intervalTime);
                // The Chrome driver does not wait for the page to load.
                if (driver.FindElements(By.CssSelector(cssSelector)).Count > 0) {
                    element = null;
                    element = driver.FindElement(By.CssSelector(cssSelector));
                    log.Write($"{crawlerName} driver.FindElement(By.CssSelector({cssSelector})");
                    element.Click();
                    log.Write($"{crawlerName} element.Click()");
                    return true;
                } else {
                    Sleep(intervalTime * 5);
                    log.Write($"no such element: Unable to locate element: {cssSelector}");
                    log.Write($"pageNumber: {pageNumber}, fileNumber: {fileNumber}");
                    WriteScreenshotDebug($"{crawlerName}_{fileNumber:000000}_ClickElementError.png");
                }            
            }
            crawlError++;
            if (crawlError > maxError) {
                throw new MaxCrawlErrorCountException();
            }
            return false;
        }

        // Click the element by XPath.
        protected bool ClickElementXPath(string xPath) {
            for (int i = 0; i < numberOfClickRetries; i++) { 
                Sleep(intervalTime);
                // The Chrome driver does not wait for the page to load.
                if (GetElementXPath(xPath)) {
                    log.Write($"{crawlerName} driver.FindElementByXPath({xPath})");
                    element.Click();
                    log.Write($"{crawlerName} element.Click()");
                    return true;
                } else {
                    Sleep(intervalTime * 5);
                    log.Write($"no such element: Unable to locate element: {xPath}");
                    log.Write($"pageNumber: {pageNumber}, fileNumber: {fileNumber}");
                    WriteScreenshotDebug($"{crawlerName}_{fileNumber:000000}_ClickElementXPathError.png");
                }            
            }
            crawlError++;
            if (crawlError > maxError) {
                throw new MaxCrawlErrorCountException();
            }
            return false;
        }

        // Input characters in the text box.
        protected void InputText(string cssSelector, string text) {
            Sleep(intervalTime);

            driver.FindElement(By.CssSelector(cssSelector)).Clear();
            driver.FindElement(By.CssSelector(cssSelector)).SendKeys(text);
            log.Write($"{crawlerName} FindElement(By.CssSelector({cssSelector}).SendKeys({text})");
        }

        // Input characters in the text box by XPath.
        protected void InputTextXPath(string xPath, string text) {
            Sleep(intervalTime);
            if(GetElementXPath(xPath)){ 
                element.Clear();
                element.SendKeys(text); 
            }
        }

        // Write HTML to a file.
        protected void WriteHtml(string htmlFileName, string directory = "") {
            if (directory.Equals("")) {
                directory = htmlDirectory;
            }

            using (var writer = new StreamWriter($"{directory}/{htmlFileName}", false, Encoding.UTF8)) {
                writer.WriteLine(html);
            }
        }

        // Write HTML to S3.
        protected void WriteHtmlS3(string bucketName, string htmlFileName) {
            string s3FileName = $"html/{crawlerName}/{htmlFileName}";
            string localFileName = $"{htmlDirectory}/{htmlFileName}";

            var request = new PutObjectRequest {
                BucketName = bucketName,
                Key = s3FileName,
                FilePath = localFileName
            };
            s3Client.PutObjectAsync(request).Wait();
        }

        // Upload directory to s3.
        protected void UplodaDirectoryS3(string bucketName, string directoryName) {
            string fileName;
            string s3FileName;
            string[] filePaths = Directory.GetFiles($"{htmlDirectory}/{directoryName}");
            
            foreach (string filePath in filePaths) { 

                fileName = Path.GetFileName(filePath);
                s3FileName = $"html/{crawlerName}/{directoryName}/{fileName}";
                
                var request = new PutObjectRequest {
                    BucketName = bucketName,
                    Key = s3FileName,
                    FilePath = filePath
                };
                s3Client.PutObjectAsync(request).Wait();
            }
        }

        // Write CSV to a file.
        protected void WriteCsv(string csvFileName) {
            using (var writer = new StreamWriter($"{csvDirectory}/{csvFileName}", true, Encoding.UTF8)) {
                writer.WriteLine(csv);
            }
        }

        // Write CSV to S3.
        protected void WriteCsvS3(string bucketName, string csvFileName) {
            string s3FileName = $"html/{crawlerName}/{csvFileName}";
            string localFileName = $"{csvDirectory}/{csvFileName}";

            var request = new PutObjectRequest {
                BucketName = bucketName,
                Key = s3FileName,
                FilePath = localFileName
            };
            s3Client.PutObjectAsync(request).Wait();
        }

        // Write TSV to S3.
        protected void WriteTsvS3(string bucketName, string tsvFileName) {
            string s3FileName = $"tsv/{crawlerName}/{tsvFileName}";
            string localFileName = $"{tsvDirectory}/{tsvFileName}";

            var request = new PutObjectRequest {
                BucketName = bucketName,
                Key = s3FileName,
                FilePath = localFileName
            };
            s3Client.PutObjectAsync(request).Wait();
        }

        // Copy Directory to S3.
        protected void CopyDirectoryS3(string directoryPath, string s3BucketName, string s3DirectoryPath) {
            string[] filePaths = System.IO.Directory.GetFiles(directoryPath);
            foreach (string filePath in filePaths) {
                string fileName = Path.GetFileName(filePath);
                string s3Key = $"{s3DirectoryPath}/{fileName}";
                var request = new PutObjectRequest {
                    BucketName = s3BucketName,
                    Key = s3Key,
                    FilePath = filePath
                };
                log.Write($"{fileName} copy S3 start.");
                s3Client.PutObjectAsync(request).Wait();
            }
        }

        // Write a screenshot for debugging.
        protected void WriteScreenshotDebug(string fileName, string directory = "") {
            if (debugMode) {
                if (directory.Equals("")) {
                    directory = htmlDirectory;
                }

                Sleep(intervalTime);
                Screenshot screenShot = driver.GetScreenshot();
                screenShot.SaveAsFile($"{directory}/{fileName}");
            }
        }

        // Write a screenshot.
        protected void WriteScreenshot(string fileName, string directory = "") {
            if (directory.Equals("")) {
                directory = htmlDirectory;
            }

            Sleep(intervalTime);
            Screenshot screenShot = driver.GetScreenshot();
            screenShot.SaveAsFile($"{directory}/{fileName}");
        }

        // Switch the screen.
        protected void SwitchWindow(string handle) {
            Sleep(intervalTime);

            driver.SwitchTo().Window(handle);
            log.Write($"{crawlerName} driver.SwitchTo().Window({handle})");
        }

        // Switch to a new screen.
        protected void SwitchNewWindow() {
            Sleep(intervalTime);

            allHandles = driver.WindowHandles;
            log.Write($"{crawlerName} driver.WindowHandles");

            int count = allHandles.Count;
            newWindow = allHandles[count - 1];  // Because the new screen is added at the end of the array.

            driver.SwitchTo().Window(newWindow);
            log.Write($"{crawlerName} SwitchTo().Window({newWindow})");
        }

        // Close the current window(tab).
        protected void CloseCurrentWindow() {
            driver.Close();
            log.Write($"{crawlerName} driver.Close()");
        }

        // Execute "Back" from the current screen.
        protected void HistoryBackWindow() {
            Sleep(intervalTime);

            driver.Navigate().Back();
            log.Write($"{crawlerName} driver.Navigate().Back()");
        }

        // sleep
        protected void Sleep(int intervalTime) {
            System.Threading.Thread.Sleep(intervalTime);
        }

        // Create a swap file.
        protected void MakeSwap() {
            // Output the interrupted file number.
            using (var writer = new StreamWriter($"{htmlDirectory}/.swap", false, Encoding.UTF8)) {
                writer.WriteLine($"{fileNumber}");
            }
        }

        // Check the swap file.
        protected bool CheckSwap() {

            string filePath = $"{htmlDirectory}/.swap";
            
            if (File.Exists(filePath)) {
                // Get the file number.
		        using (StreamReader streamReader = new StreamReader (filePath, Encoding.UTF8)) {
				    string line = streamReader.ReadToEnd();
                    restartFiledNumber = int.Parse(line);
		        }
                // Set resume mode to true.
                restartMode = true;
                // Delete the swap file.
                File.Delete(filePath);
                log.Write($"{crawlerName} restart from #{restartFiledNumber}.");
                return true;
            } else {
                return false;
            }
        }

        // Allow drivers to download CSV and more.
        protected void SetDownloadBehavior(string downloadPath = "") {
            if (downloadPath == "") { 
                downloadPath = htmlDirectory;
            }
            if (Environment.OSVersion.Platform != PlatformID.Unix) {
                // The path to be set in the driver's Chrome command must be specified with "\" instead of "/".
                downloadPath = downloadPath.Replace("/", "\\");
            }
            Directory.CreateDirectory(downloadPath);

            var paramList = new Dictionary<string, object>();
            paramList.Add("behavior", "allow");
            paramList.Add("downloadPath", downloadPath);
            driver.ExecuteCdpCommand("Page.setDownloadBehavior", paramList);
        }

        // Execute JavaScript.
        protected void ExecuteJavaScript(string script) { 
            Sleep(intervalTime);
            driver.ExecuteScript(script);
        }

        // Check if the element exists.
        protected bool CheckElement(string cssSelector) { 
            if (driver.FindElements(By.CssSelector(cssSelector)).Count > 0) {
                return true;
            }
            return false;
        }

        // Check if the element exists by XPath.
        protected bool CheckElementXPath(string xPath) { 
            if (GetElementXPath(xPath)) {
                return true;
            }
            return false;
        }
        
        // Check if a pop-up alert is displayed.
        public bool CheckAlert() {
            bool presentFlag = false;
            try {
                IAlert alert = driver.SwitchTo().Alert();
                presentFlag = true;
                log.Write($"{crawlerName} alert text: {alert.Text}");
                alert.Accept();
            } catch (NoAlertPresentException ex) {
                log.Write($"{crawlerName} {ex.Message}");
            }
            return presentFlag;
        }

        // Open a new window
        public bool OpenNewWindow(string url) { 
            try {
                Sleep(intervalTime);
                ExecuteJavaScript("window.open()");
                SwitchNewWindow();
                GetPage(url);
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return false;
            }
            return true;
        }

        // Get elements by css selector.
        protected int GetElements(string cssSelector) {
            elements = null;
            try { 
                elements = driver.FindElements(By.CssSelector(cssSelector));
                return elements.Count;
            } catch { 
                return -1;       
            }
        }

        // Get elements by css selector.
        protected bool GetElement(string cssSelector) {
            element = null;
            try {
                element = driver.FindElement(By.CssSelector(cssSelector));
            } catch { 
                return false;
            }
            return true;
        }

        // Get elements by XPath.
        protected bool GetElementXPath(string xPath) {
            element = null;
            try {
                element = driver.FindElement(By.XPath(xPath));
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return false;
            }
            return true;
        }

        protected IWebElement ReturnElementXPath(string xPath) {
            IWebElement webElement = null;
            try {
                webElement = driver.FindElement(By.XPath(xPath));
                return webElement;
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return webElement;
            }
        }

        // Get elements by XPath.
        protected int GetElementsXPath(string xPath) {
            elements = null;
            try {
                elements = driver.FindElements(By.XPath(xPath));
                return elements.Count;
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return -1;
            }
        }

        // Retrun elements by XPath.
        protected ReadOnlyCollection<IWebElement> ReturnElementsXPath(string xPath) {
            ReadOnlyCollection<IWebElement> webElements = null;
            try {
                webElements = driver.FindElements(By.XPath(xPath));
                return webElements;
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return webElements;
            }
        }

        // Get the string of the element.
        protected string GetElementString(string cssSelector) {
            string s = "";
            element = null;
            try {
                element = driver.FindElement(By.CssSelector(cssSelector));
                s = ReplaceControlCode(element.Text);
                return s;
            } catch {
                return s;
            }
        }

        // Get the string of the element by XPath.
        protected string GetElementStringXpath(string xPath) {
            string s = "";
            element = null;
            try {
                element = driver.FindElement(By.XPath(xPath));
                s = ReplaceControlCode(element.Text);
                return s;
            } catch {
                return s;
            }
        }

        // Replace the controle code.
        private string ReplaceControlCode(string s) { 
            string d = s;
            d = d.Replace("\t", " ");
            d = d.Replace("\r\n", " ");
            d = d.Replace("\r", " ");
            d = d.Replace("\n", " ");
            return d;
        }

        // Get the value of an element as an int.
        protected int GetElementInt(string cssSelector) {
            element = null;
            try {
                element = driver.FindElement(By.CssSelector(cssSelector));
                string s = element.Text;
                int i = int.Parse(s);
                return i;
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return -1;
            }
        }

        // Get the coordinates from the href attribute string.
        public (decimal, decimal) GetCoodinate(string css) { 
            try { 
                GetElement(css); 
                string href = element.GetAttribute("href");
                Regex regex = new Regex(@"\d+\.\d+");
                MatchCollection matches = regex.Matches(href);

                decimal latitude = decimal.Parse(matches[0].Value);
                decimal longitude = decimal.Parse(matches[1].Value);
                return (latitude, longitude);        
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
                return (-1M, -1M);        
            }
        }

        // Enter characters.
        public void EnterCharacters(string css, string input){
            try {
                GetElement(css);
                Sleep(intervalTime);
                element.SendKeys(input);
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");
            }
        }
        
        // Delete unnecessary tags such as script tags, link tags, and style tags.
        public virtual void DeleteUnnecessaryTags(string srcFile, string destFile){

            string localHtml = "";

            using(StreamReader fs = new StreamReader(srcFile, Encoding.UTF8)) { 
                localHtml = fs.ReadToEnd();
                localHtml = Regex.Replace(localHtml, @"<script.*?>.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);    
                localHtml = Regex.Replace(localHtml, @"<style.*?>.*?</style>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
                localHtml = Regex.Replace(localHtml, @"<link.*?>", "", RegexOptions.IgnoreCase);    
                localHtml = Regex.Replace(localHtml, @"<meta.*?>", "", RegexOptions.IgnoreCase);    
                localHtml = Regex.Replace(localHtml, @"<img.*?>", "", RegexOptions.IgnoreCase);   
                localHtml = Regex.Replace(localHtml, @"onload="".*?""", "", RegexOptions.IgnoreCase);
                localHtml = Regex.Replace(localHtml, @"style="".*?""", "", RegexOptions.IgnoreCase);
            }

            using (StreamWriter fd = new StreamWriter(destFile, false, Encoding.UTF8)) {
                fd.WriteLine(localHtml);
            }
        }

        // Get file number from file name.
        public int GetFileNumber(string fileName) { 
            int fileNumber = 0;
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(fileName);
            fileNumber = int.Parse(match.Value);
            return fileNumber;
        }

        // Extract the compressed file with 7-Zip
        // (Because System.IO.Compression.ZipFile of .NET Core cannot handle Shift-JIS.)
        public void ExtractWithSevenZip(string source, string destination, string sevenZipPath = "C:/Program Files/7-Zip/7zG.exe") {
            if (!Directory.Exists(destination)) {
                Directory.CreateDirectory(destination);
            }

            try {
                ProcessStartInfo processStartInfo = new ProcessStartInfo {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = sevenZipPath,
                    Arguments = "x \"" + source + "\" -o" + destination
                };
                Process process = Process.Start(processStartInfo);
                process.WaitForExit();
            } catch (Exception ex) {
                log.Write($"{crawlerName} {ex.Message}");      
            }
        }
        // Converts a byte array to a hexadecimal string.
        public string ByteArrayToString(byte[] arrInput) {
            StringBuilder sOutput = new StringBuilder(arrInput.Length);
            for (int i = 0; i < arrInput.Length - 1; i++) {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
            return sOutput.ToString();
        }

        //　Create answers file.
        public void CreateAnswerFile(string version) {
			Directory.CreateDirectory(answersDirectory);

            DateTime date = DateTime.Now;
            string dateString = date.ToString("yyyy-MM-dd HH:mm:ss");
            string comment = $@"#
# Answers of {crawlerName} testing
# v{version} {dateString}
#
";

            using (var writer = new StreamWriter($"{answersDirectory}/v{version}.yml", false, Encoding.UTF8)) {
                writer.WriteLine(comment);
            }
        }

        //　Save the answer.
        public void WriteAnswer(string version, string key, string hash) {
            string line = $"{key}: {hash}";
            using (var writer = new StreamWriter($"{answersDirectory}/v{version}.yml", true, Encoding.UTF8)) {
                writer.WriteLine(line);
            }
        }

        // Calculate hash.
        public string CalculateHash() {
            byte[] htmlByteArray = Encoding.UTF8.GetBytes(html);
            byte[] hashByteArray = new HMACMD5().ComputeHash(htmlByteArray);
            string hashString = ByteArrayToString(hashByteArray);
            return hashString;
        }

        // Delete files in directory.
        public void DeleteFilesInDirectory(string directoryPath) {
            string[] filePaths = Directory.GetFiles(directoryPath);
            foreach (string filePath in filePaths) {
                FileInfo file = new FileInfo(filePath);
                file.Delete();
            }
        }

        // Delete directory.
        public void DeleteDirectoryRecursive(string directoryPath) {
            if (!Directory.Exists(directoryPath)) {
                return;
            }

            // Delete files.
            string[] filePaths = Directory.GetFiles(directoryPath);
            foreach (string filePath in filePaths) {
                File.SetAttributes(filePath, FileAttributes.Normal);
                File.Delete(filePath);
            }

            // Delete directories.
            string[] innerDirectoryPaths = Directory.GetDirectories(directoryPath);
            foreach (string innerDirectoryPath in innerDirectoryPaths) {
                DeleteDirectoryRecursive(innerDirectoryPath);
            }

            // Delete directory.
            Directory.Delete(directoryPath, false);
        }
    }
}
