﻿using System.IO;

namespace Nona
{

    public interface IModelTsv {
        string WriteTsv(StreamWriter writer);
        void WriteTsvHeader(StreamWriter writer);
    }

}
