﻿using System;

namespace Nona
{
    public class MaxCrawlErrorCountException : Exception {

        public MaxCrawlErrorCountException() : base("The number of errors has exceeded the maximum number of errors.") {
        }

        public MaxCrawlErrorCountException(string message) : base(message) {
        }

        public MaxCrawlErrorCountException(string message, Exception innerException) : base(message, innerException) {
        }
    }
}
